﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DropdownImage : MonoBehaviour
{
    public Dropdown ImageDropdown;

    public GameObject HumanImage;
    public GameObject CarImage;
    public GameObject HouseImage;
    // Start is called before the first frame update
    void Start()
    {
        HumanImage.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ImageShowUp()
    {
        if (ImageDropdown.value == 0)
        {
            HumanImage.SetActive(true);
            CarImage.SetActive(false);
            HouseImage.SetActive(false);
        }
        if (ImageDropdown.value == 1)
        {
            HumanImage.SetActive(false);
            CarImage.SetActive(true);
            HouseImage.SetActive(false);
        }
        if (ImageDropdown.value == 2)
        {
            HumanImage.SetActive(false);
            CarImage.SetActive(false);
            HouseImage.SetActive(true);
        }
    }
}
