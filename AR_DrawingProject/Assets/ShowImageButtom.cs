﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using  UnityEngine.UI;
public class ShowImageButtom : MonoBehaviour
{
    public GameObject ShowImagePanel;
    public GameObject ShowImage;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OpenImagePanel()
    {
        ShowImage.SetActive(true);
        ShowImagePanel.SetActive(false);
    }
}
