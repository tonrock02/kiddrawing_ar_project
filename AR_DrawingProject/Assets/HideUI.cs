﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HideUI : MonoBehaviour
{
    //public GameObject ImagePanel;
    public Text BottonText;
    public bool CheckUIBotton;
    public GameObject Image1;
    public GameObject Image2;
    public GameObject Image3;
    public GameObject Image4;
    public GameObject Image5;
    public GameObject Image6;
    public GameObject ShowImageButtom;
    public GameObject HelpButton;
    public GameObject HelpPanel;

    //public GameObject DropdownImage;
    public void Start()
    {
        CheckUIBotton = true;
    }

    public void UIManager()
    {
        if (CheckUIBotton == true)
        {
            //ImagePanel.SetActive(false);
            //DropdownImage.SetActive(false);
            Image1.SetActive(false);
            Image2.SetActive(false);
            Image3.SetActive(false);
            Image4.SetActive(false);
            Image5.SetActive(false);
            Image6.SetActive(false);
            ShowImageButtom.SetActive(false);
            CheckUIBotton = false;
            HelpButton.SetActive(false);
            HelpPanel.SetActive(false);
            BottonText.text = "OPEN UI";
        }
        else
        {
            //ImagePanel.SetActive(true);
            //DropdownImage.SetActive(true);
            ShowImageButtom.SetActive(true);
            HelpButton.SetActive(true);
            CheckUIBotton = true;
            BottonText.text = "HIDE UI";
        }
    }
}
